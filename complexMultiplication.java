/*
This is a question from leetcode on the multiplication of complex numbers.

You need to return a string representing their multiplication. Note i2 = -1 according to the definition.
*/
class Solution {
	public static int[] getNumv3(String t) {
		int PLUSINDEX = t.indexOf("+");
		int[] x = new int[2];
		x[0] = Integer.valueOf(t.substring(0, PLUSINDEX));
		x[1] = Integer.valueOf(t.substring(PLUSINDEX+1, t.length()-1));
		return x;
	}
	public String complexNumberMultiply(String a, String b) {
		int[] xa = getNumv3(a);
		int[] xb = getNumv3(b);
		int[] result = new int[2];
		result[0]=xa[0]*xb[0]-xa[1]*xb[1];
		result[1]=xa[0]*xb[1]+xa[1]*xb[0];
		StringBuilder sb = new StringBuilder();
		sb.append(result[0]);sb.append('+');sb.append(result[1]);sb.append('i');
		return sb.toString();
	}
}